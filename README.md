# Software Engineer golang test

Create application that saves timestamp (and other required data) to a database of your choice. The application should use routines for workers that will be created via REST API. For example, a POST request to `/create-worker` with body: 

```
{
  "frequency": 2,
  "granularity": "seconds"
}
```

will create a worker that creates a timestamp entry in the database every two seconds. Additionally, the application should allow couting the timestamp rows by worker Id, listing existing workers and closing the ones that are running via REST API endpoints.

Make sure you provide unit tests, how to run and documentation for your code. 

Not essential but nice to see:

- running in docker
- ci/cd

